//
//  GPSTracker.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 11/2/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation

/**
 Activity tracker that records GPS locations
*/
class GPSTracker: NSObject, Tracker, CLLocationManagerDelegate {
    
    static let distanceFilter = 10.0
    static let maximumError = 20.0
    
    private var m_data: GPSTrackerData
    
    private var lastLocation: CLLocation? = nil
    private var eventTimes: [Date] = [Date]()
    
    var data: TrackerData {
        get {
            return m_data
        }
    }
    
    private var locationManager: CLLocationManager
    
    init(for activity: Activity) {
        m_data = NSEntityDescription.insertNewObject(forEntityName: TrackerType.GPS.trackerDataEntityName, into: AppDelegate.delegate.persistentContainer.viewContext) as! GPSTrackerData
        m_data.initialize(activity: activity)
        locationManager = CLLocationManager()
        
        super.init()
        
        // Initialise the location manager
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.activityType = .fitness
        locationManager.requestAlwaysAuthorization()
    }
    
    // MARK: Tracker
    
    func activityStarted() {
        locationManager.startUpdatingLocation()
        
        startedEvent()
    }
    
    func activityPaused() {
        stoppedEvent()
    }
    
    func activityEnded() {
        stoppedEvent()
        
        locationManager.stopUpdatingLocation()
    }
    
    // MARK: Managing times
    
    func startedEvent() {
        // Check that the activity is not already started
        assert(eventTimes.count % 2 == 0)
        // Add this event
        eventTimes.append(Date())
    }
    
    func stoppedEvent() {
        // Check that the activity is running
        assert(eventTimes.count % 2 == 1)
        // Add this event
        eventTimes.append(Date())
    }
    
    func isStartedAt(time: Date) -> Bool {
        // Check its not from before the activity started
        if (eventTimes.isEmpty || time < eventTimes.first!) {
            return false
        }
        // Check it doesn't come at a time the activity was paused/ended
        for i in stride(from: 1, to: eventTimes.count, by: 2) {
            // If its after an end/pause event and before the following start event
            if time > eventTimes[i] && (i + 1 >= eventTimes.count || time < eventTimes[i + 1]) {
                return false
            }
        }
        return true
    }
    
    // MARK: Location Manager
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for location in locations {
            
            
            // Check this is not the first seen location, and its for a time when the activity was started
            if let last = lastLocation, isStartedAt(time: location.timestamp) {
                // Check the new location is more recent
                if location.timestamp >= last.timestamp {
                    // Add the distance from the previous location to the cumulative distance cache
                    m_data.distanceCache += location.distance(from: last)
                }
            }
            
            // Update the lastLocation if newer and not before starting
            if !eventTimes.isEmpty && location.timestamp >= eventTimes.first! {
                if lastLocation == nil || location.timestamp >= lastLocation!.timestamp {
                    lastLocation = location
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
}
