//
//  ActivityType.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 24/1/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

class ActivityType {
    // String localisation key
    public private(set) var name: String

    // Set of supported tracker types
    public private(set) var trackerTypes: Set<TrackerType>

    // Closure to calculate the number of Calories burnt for the specified activity of this type
    public private(set) var calculateCalories: (Activity) -> Float

    init(name: String, trackerTypes types: Set<TrackerType>, caloryCalculator calc: @escaping (Activity) -> Float) {
        self.name = name
        self.trackerTypes = types
        self.calculateCalories = calc
    }

    static let types: [String: ActivityType] = [
        "run": ActivityType(name: "activity_run", trackerTypes: [.Time, .GPS], caloryCalculator: { activity in 100 }),
        "walk": ActivityType(name: "activity_walk", trackerTypes: [.Time, .GPS], caloryCalculator: { activity in 100 })
    ]
}
