//
//  TrackerTypeTableViewController.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 2/2/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import UIKit

class TrackerTypeTableViewController: UITableViewController {

    static let cellReuseIdentifier = "trackerType"
    
    var trackerTypes: [TrackerType: Bool]? {
        didSet {
            if let types = trackerTypes {
                trackerTypeArray = Array(types.keys)
            }
        }
    }
    var enabledChanged: ((TrackerType, Bool) -> Void)?
    
    // Ordered version of available tracker types
    private var trackerTypeArray: [TrackerType]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return trackerTypeArray?.count ?? 0
        }
        else {
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TrackerTypeTableViewController.cellReuseIdentifier, for: indexPath)
        
        if let type = trackerTypeArray?[indexPath.row] {
            cell.textLabel?.text = type.description
            
            if !(cell.accessoryView is UISwitch) {
                // Put a switch in the accessory view
                let sw = UISwitch()
                sw.addTarget(self, action: #selector(switchChanged(sw:)), for: .valueChanged)
                cell.accessoryView = sw
            }
            // Update the new or existing UISwitch
            let sw = cell.accessoryView as! UISwitch
            sw.isOn = trackerTypes?[type] ?? false
            sw.tag = indexPath.row
        }

        return cell
    }
    
    func switchChanged(sw: UISwitch) {
        let row = sw.tag
        if let type = trackerTypeArray?[row], trackerTypes?[type] != nil {
            enabledChanged?(type, sw.isOn)
        }
    }
}
