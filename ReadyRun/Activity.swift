//
//  Activity.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 26/1/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import Foundation
import CoreData

class Activity: NSManagedObject {
    static let entityName = "Activity"
    
    public var type: ActivityType? {
        get {
            // Get the ActivityType object based on the stored string
            return ActivityType.types[self.typeString ?? ""]
        }
    }
    
    public func getTrackerData(trackerType type: TrackerType) -> TrackerData? {
        if let dataSet = self.trackerData {
            return dataSet.first(where: { (data) in (data as! TrackerData).activity == self }) as? TrackerData
        }
        return nil
    }
}
