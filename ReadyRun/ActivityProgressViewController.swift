//
//  ActivityProgressViewController.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 9/2/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import UIKit
import CoreData

class ActivityProgressViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    static let cellReuseIdentifier = "value"
    static let valueUpdateInterval = 1.0
    
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    let cellSpacing: CGFloat = 2
    let valueColumns = 2
    
    private var activity: Activity!
    private var trackers: [TrackerType: Tracker]!
    
    // List of info to show, each with closure for calculating displayed value.
    private var values: [ActivityValue]!
    
    private var paused = true {
        didSet {
            updatePausedButton()
        }
    }
    
    private var valueUpdateTimer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        
        startActivity()
        
        valueUpdateTimer = Timer.scheduledTimer(withTimeInterval: ActivityProgressViewController.valueUpdateInterval, repeats: true) {[weak self] _ in
            self?.collectionView.reloadData()
        }
    }
    
    func setActivity(activityType: String, trackerTypes: [TrackerType]) {
        // Insert the activity and set its details
        activity = NSEntityDescription.insertNewObject(forEntityName: Activity.entityName, into: AppDelegate.delegate.persistentContainer.viewContext) as! Activity
        activity.typeString = activityType
        
        // Create trackers
        trackers = [TrackerType: Tracker]()
        for trackerType in trackerTypes {
            let tracker = trackerType.createTracker(for: activity)
            trackers[trackerType] = tracker
        }
        
        updateValueList()
    }
    
    // Update the set of values that can be displayed with the current trackers
    private func updateValueList() {
        values = ActivityValue.values.values.filter() {
            return $0.supports(trackerTypes: Array(trackers.keys))
        }
        // TODO Probably want to be able to configure desired values to show
    }
    
    private func startActivity() {
        activity.startTime = NSDate();
        paused = false
        
        // Start trackers
        for tracker in trackers.values {
            tracker.activityStarted()
        }
    }
    
    private func pauseResumeActivity() {
        // Update paused state
        paused = !paused
        // Pause or unpause trackers
        for tracker in trackers.values {
            if paused {
                tracker.activityPaused()
            }
            else {
                tracker.activityStarted()
            }
        }
    }
    
    private func endActivity() {
        // End trackers
        for tracker in trackers.values {
            tracker.activityEnded()
        }
        saveProgress()
    }
    
    private func saveProgress() {
        // Tell the TrackerData objects to store their data in CoreData attributes
        for tracker in trackers.values {
            tracker.data.storeData()
        }
        
        AppDelegate.delegate.saveContext()
    }
    
    private func updatePausedButton() {
        collectionView.reloadData()
        // Update pause button text
        if paused {
            pauseButton.setTitle(NSLocalizedString("resume_button", comment: "Resume paused activity button title"), for: .normal)
        }
        else {
            pauseButton.setTitle(NSLocalizedString("pause_button", comment: "Pause activity button title"), for: .normal)
        }
    }
    
    private func close() {
        valueUpdateTimer?.invalidate()
        valueUpdateTimer = nil
        self.dismiss(animated: true)
    }
    
    // MARK: - Button actions
    
    @IBAction func cancelPressed(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: NSLocalizedString("cancel_activity", comment: "Cancel alert title"), message: NSLocalizedString("cancel_message", comment: "Cancel alert message"), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("no_action", comment: "No alert option"), style: .default))
        alert.addAction(UIAlertAction(title: NSLocalizedString("yes_action", comment: "Yes alert option"), style: .default) { (action) in
            // TODO cancel activity
            self.close()
        })
        present(alert, animated: true)
    }
    
    @IBAction func pausePressed(_ sender: UIButton) {
        pauseResumeActivity()
    }
    
    @IBAction func finishPressed(_ sender: UIButton) {
        let alert = UIAlertController(title: NSLocalizedString("finish_activity", comment: "Finish activity alert title"), message: NSLocalizedString("finish_activity_message", comment: "Finish activity alert message"), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("no_action", comment: "No alert option"), style: .default))
        alert.addAction(UIAlertAction(title: NSLocalizedString("yes_action", comment: "Yes alert option"), style: .default) { (action) in
            self.endActivity()
            self.close()
        })
        present(alert, animated: true)
    }
    
    // MARK: - Collection View
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return values?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ActivityProgressViewController.cellReuseIdentifier, for: indexPath)
        
        if let valueCell = cell as? ValueCollectionViewCell {
            let activityValue = values[indexPath.row]
            valueCell.descriptionLabel.text = NSLocalizedString(activityValue.description, comment: "Tracker info description")
            valueCell.valueLabel.text = activityValue.format(value: activityValue.getValue(from: trackers))
        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.bounds.width - cellSpacing) / CGFloat(valueColumns), height: (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).itemSize.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return cellSpacing
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
