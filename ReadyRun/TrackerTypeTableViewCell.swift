//
//  TrackerTypeTableViewCell.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 2/2/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import UIKit

class TrackerTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var enableSwitch: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
