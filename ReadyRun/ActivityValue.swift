//
//  ActivityValue.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 11/2/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import Foundation

/**
 Describes a value associated with an activity that can be computed from trackers or trackerdata.
 
 This allowes raw data stored by TrackerData objects to be converted to useful information and formatted.
*/
class ActivityValue {
    let description: String
    private let requiredTrackers: [TrackerType]
    private let value: ([TrackerType: TrackerData]) -> Double
    private let formatter: (Double) -> String
    
    init(description: String, requires trackers: [TrackerType], get value: @escaping ([TrackerType: TrackerData]) -> Double, format formatter: @escaping (Double) -> String) {
        self.description = description
        self.value = value
        self.formatter = formatter
        self.requiredTrackers = trackers
    }
    
    func supports(trackerTypes types: [TrackerType]) -> Bool {
        for required in requiredTrackers {
            if !types.contains(required) {
                return false
            }
        }
        return true
    }
    
    func supports(activity: Activity) -> Bool {
        guard let data = activity.trackerData as? Set<TrackerData> else {
            return false
        }
        return supports(trackerTypes: data.map() {return $0.type})
    }
    
    func getValue(from trackerData: [TrackerType: TrackerData]) -> Double {
        return value(trackerData)
    }
    
    func getValue(from trackerData: [TrackerData]) -> Double {
        var dict = [TrackerType: TrackerData]()
        for data in trackerData {
            dict[data.type] = data
        }
        return getValue(from: dict)
    }
    
    func getValue(from trackers: [TrackerType: Tracker]) -> Double {
        return getValue(from: trackers.values.map() { $0.data })
    }
    
    func format(value: Double) -> String {
        return formatter(value)
    }
    
    static let values = [
        "total_time": ActivityValue(description: "time", requires: [.Time], get: { return ($0[.Time] as! TimeTrackerData).getDuration() }, format: {
            let formatter = DateComponentsFormatter()
            formatter.unitsStyle = .abbreviated
            return formatter.string(from: $0) ?? "--"
        }),
        "distance_km": ActivityValue(description: "distance", requires: [.GPS], get: { return ($0[.GPS] as! GPSTrackerData).calculateDistance() * 0.001 }, format: {String(format: NSLocalizedString("distance_value_km", comment: "Activity km distance format"), $0)}),
        "distance_mi": ActivityValue(description: "distance", requires: [.GPS], get: { return ($0[.GPS] as! GPSTrackerData).calculateDistance() * 0.000621371 }, format: {String(format: NSLocalizedString("distance_value_mi", comment: "Activity miles distance format"), $0)}),
        "pace_mpk": ActivityValue(description: "pace", requires: [.Time, .GPS], get: {
            let seconds = ($0[.Time] as! TimeTrackerData).getDuration()
            let metres = ($0[.GPS] as! GPSTrackerData).calculateDistance()
            return 16.666666666 * seconds / metres
        }, format: {
            let formatter = DateComponentsFormatter()
            formatter.allowedUnits = [.minute, .second]
            formatter.unitsStyle = .positional
            formatter.zeroFormattingBehavior = .pad
            let value = ($0.isNaN || !$0.isFinite) ? 0.0 : $0
            return String(format: NSLocalizedString("pace_value_mpk", comment: "Activity pace min/km format"), formatter.string(from: value*60) ?? "--")
        })
    ]
}
