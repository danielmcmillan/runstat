//
//  StartActivityTableViewController.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 31/1/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import UIKit

class StartActivityTableViewController: UITableViewController {
    
    struct SegueIdentifiers {
        static let showActivityTypes = "showActivityTypes"
        static let showTrackerTypes = "showTrackerTypes"
        static let startActivity = "startActivity"
    }
    
    @IBOutlet weak var typeCell: UITableViewCell!
    @IBOutlet weak var trackersCell: UITableViewCell!
    
    // The String key corresponding to the selected ActivityType in ActivityType.types
    var activityType: String! {
        didSet {
            trackerTypes = [TrackerType: Bool]()
            // Default to all tracker types supported by the activity type
            for trackerType in ActivityType.types[activityType]!.trackerTypes {
                trackerTypes[trackerType] = true
            }
            updateUI()
        }
    }
    var trackerTypes: [TrackerType: Bool]!

    override func viewDidLoad() {
        super.viewDidLoad()
        activityType = ActivityType.types.first!.key
    }
    
    func updateUI() {
        typeCell.textLabel?.text = NSLocalizedString(ActivityType.types[activityType]!.name, comment: "Activity type name")
        
        // Comma separated list of the selected tracker types
        let selectedTrackerTypes = trackerTypes.filter(){(key, value) in value}.flatMap(){$0.key.description}
        if selectedTrackerTypes.isEmpty {
            trackersCell.textLabel?.text = NSLocalizedString("no_tracker", comment: "Empty tracker list description")
        }
        else {
            trackersCell.textLabel?.text = selectedTrackerTypes.joined(separator: ", ")
        }
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let identifier = segue.identifier {
            if identifier == SegueIdentifiers.showActivityTypes {
                if let destination = segue.destination as? ActivityTypeTableViewController {
                    destination.selectedActivityType = self.activityType
                    // Tell the ActivityType table to update self.selectedType when an option is chosen
                    destination.chose = { (type) in self.activityType = type }
                }
            }
            else if identifier == SegueIdentifiers.showTrackerTypes {
                if let destination = segue.destination as? TrackerTypeTableViewController {
                    destination.trackerTypes = trackerTypes
                    destination.enabledChanged = {(type, enabled) in
                        self.trackerTypes[type] = enabled
                        self.updateUI()
                    }
                }
            }
            else if identifier == SegueIdentifiers.startActivity {
                if let nav = segue.destination as? UINavigationController, let destination = nav.topViewController as? ActivityProgressViewController {
                    // Give the activity progress controller the chosen activity type and a list of enabled tracker types
                    destination.setActivity(activityType: activityType, trackerTypes: trackerTypes.filter() { item in return item.value }.map() { item in return item.key })
                }
            }
        }
    }
}
