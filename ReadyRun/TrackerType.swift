//
//  TrackerType.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 24/1/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import UIKit

enum TrackerType: Int16, CustomStringConvertible {
    case Time = 0
    case GPS
    //case Steps
    
    var description: String {
        switch self {
        case .Time:
            return NSLocalizedString("tracker_time", comment: "Time Tracker Type")
        case .GPS:
            return NSLocalizedString("tracker_gps", comment: "GPS Tracker Type")
        /*case .Steps:
            return NSLocalizedString("tracker_steps", comment: "Steps Tracker Type")*/
        }
    }
    
    var trackerDataEntityName: String {
        switch self {
        case .Time:
            return TimeTrackerData.entityName
        case .GPS:
            return GPSTrackerData.entityName
        }
    }
    
    func createTracker(for activity: Activity) -> Tracker {
        var tracker: Tracker;
        switch self {
        case .Time:
            tracker = TimeTracker(for: activity)
        case .GPS:
            tracker = GPSTracker(for: activity)
        }
        return tracker
    }
}
