//
//  GPSTrackerData.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 27/1/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import Foundation
import CoreData

class GPSTrackerData: TrackerData {
    static let entityName = "GPSTrackerData"
    
    override func storeData() {
        super.storeData()
    }
    
    override var type: TrackerType {
        get {
            return TrackerType.GPS;
        }
    }
    
    // MARK: Convenience methods for retrieving data
    
    func calculateDistance() -> Double {
        return distanceCache
    }
}
