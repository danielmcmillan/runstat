//
//  ValueCollectionViewCell.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 9/2/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import UIKit

class ValueCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
}
