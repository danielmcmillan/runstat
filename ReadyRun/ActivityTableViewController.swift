//
//  ActivityTableViewController.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 29/4/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import UIKit
import CoreData

class ActivityTableViewController: UITableViewController {

    static let cellReuseIdentifier = "activity"
    
    var resultsController: NSFetchedResultsController<Activity>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        let request: NSFetchRequest<Activity> = Activity.fetchRequest() //NSFetchRequest<Activity>(entityName: Activity.entityName)
        let dateSort = NSSortDescriptor(key: "startTime", ascending: false)
        request.sortDescriptors = [dateSort]
        resultsController = NSFetchedResultsController<Activity>(fetchRequest: request, managedObjectContext: AppDelegate.delegate.persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        // resultsController.delegate = self

        do {
            try resultsController.performFetch()
        }
        catch {
            fatalError("Failed to initialize FetchedResultsController: \(error)")
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return resultsController.sections!.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultsController.sections?[section].numberOfObjects ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ActivityTableViewController.cellReuseIdentifier, for: indexPath)
        
        let activity = resultsController.object(at: indexPath)
        
        // Create formatter for activity date
        let formatter = DateFormatter()
        formatter.dateStyle = .full
        
        // Get the name of the activity type
        let typeString = NSLocalizedString(ActivityType.types[activity.typeString!]!.name, comment: "Activity type string")
        
        cell.textLabel?.text = typeString + " - " + formatter.string(from: activity.startTime! as Date)
        
        // Set the cell detail to tracker data values
        var detail = ""
        var firstValue = true
        for value in ActivityValue.values.values {
            if value.supports(activity: activity) {
                if !firstValue {
                    detail += ", "
                }
                detail += value.format(value: value.getValue(from: Array(activity.trackerData!) as! [TrackerData]))
                firstValue = false
            }
        }
        
        cell.detailTextLabel?.text = detail

        return cell
    }

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
