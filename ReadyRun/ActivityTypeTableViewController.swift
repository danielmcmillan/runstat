//
//  ActivityTypeTableViewController.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 2/2/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import UIKit

class ActivityTypeTableViewController: UITableViewController {

    static let cellReuseIdentifier = "activityType"
    
    // Get an ordering for the activity types
    let activityTypes = Array(ActivityType.types.keys)
    // The activity that was previously selected (checkmark shown on it)
    var selectedActivityType: String?
    // Closure called when a selection is made
    var chose: ((String) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return activityTypes.count
        }
        else {
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ActivityTypeTableViewController.cellReuseIdentifier, for: indexPath)
        // Set text to the name of the activity type
        cell.textLabel?.text = NSLocalizedString(ActivityType.types[activityTypes[indexPath.row]]!.name, comment: "Activity type name")
        cell.accessoryType = (selectedActivityType == activityTypes[indexPath.row]) ? .checkmark : .none
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.chose?(self.activityTypes[indexPath.row])
        if let nav = navigationController {
            nav.popViewController(animated: true)
        }
        else {
            dismiss(animated: true)
        }
    }
}
