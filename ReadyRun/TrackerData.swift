//
//  TrackerData.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 26/1/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import Foundation
import CoreData

/**
 Abstract class.
 Subclasses store data collected by a tracker, both temporarily and in CoreData attributes
*/
class TrackerData: NSManagedObject {
    
    /// Initialise the tracker data for a new activity
    func initialize(activity: Activity) {
        self.activity = activity
    }
    
    /// Store collected data in CoreData attributes
    func storeData() { }
    
    /// The type of tracker that data is being stored for
    var type: TrackerType {
        get {
            preconditionFailure("This method must be overriden")
        }
    }
}
