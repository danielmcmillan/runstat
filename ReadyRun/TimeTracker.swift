//
//  GPSTracker.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 14/2/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import Foundation
import CoreData

/**
 Time tracker that records activity duration
*/
class TimeTracker: Tracker {
    
    private var m_data: TimeTrackerData
    private var started = false
    
    var data: TrackerData {
        get {
            return m_data
        }
    }
    
    init(for activity: Activity) {
        m_data = NSEntityDescription.insertNewObject(forEntityName: TrackerType.Time.trackerDataEntityName, into: AppDelegate.delegate.persistentContainer.viewContext) as! TimeTrackerData
        data.initialize(activity: activity)
    }
    
    func activityStarted() {
        precondition(!started)
        // Starting a new activity period
        recordCurrentTime();
        started = true
    }
    
    func activityPaused() {
        precondition(started)
        // Record the pause event
        recordCurrentTime()
        started = false
    }
    
    func activityEnded() {
        if started {
            // Record activity end event
            recordCurrentTime()
        }
    }
    
    // Record the current time in the tracker data
    private func recordCurrentTime() {
        let time = NSDate.timeIntervalSinceReferenceDate
        m_data.addEventTime(time: time)
    }
}
