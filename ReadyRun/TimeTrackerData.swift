//
//  GPSTrackerData.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 27/1/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import Foundation
import CoreData

class TimeTrackerData: TrackerData {
    static let entityName = "TimeTrackerData"
    
    private var eventTimes: [TimeInterval]?
    private var duration: Double = 0
    
    override func initialize(activity: Activity) {
        super.initialize(activity: activity)
        eventTimes = [TimeInterval]()
    }
    
    override func storeData() {
        super.storeData()
        
        self.durationCache = duration
        
        if var times = eventTimes {
            if times.count % 2 != 0 {
                // Last period has not ended, add current time
                times.append(NSDate.timeIntervalSinceReferenceDate)
                self.durationCache += times.last! - times[times.count - 2]
            }
            // Store the times array as binary data
            self.eventTimesArray = NSKeyedArchiver.archivedData(withRootObject: times) as NSData
        }
    }
    
    override var type: TrackerType {
        get {
            return TrackerType.Time;
        }
    }
    
    func addEventTime(time: Double) {
        eventTimes!.append(time)
        
        if eventTimes!.count % 2 == 0 {
            // Completed a period, add to duration
            duration += time - eventTimes![eventTimes!.count - 2]
        }
    }
    
    // Load times from CoreData attributes if required
    private func loadEventTimes() {
        if eventTimes == nil, let array = eventTimesArray {
            eventTimes = (NSKeyedUnarchiver.unarchiveObject(with: array as Data)! as! [TimeInterval])
            duration = self.durationCache
        }
    }
    
    // MARK: Convenience methods for retrieving data
    
    /// Returns the duration (time unpaused) in seconds
    func getDuration() -> Double {
        loadEventTimes()
        guard let times = eventTimes else {
            return 0
        }
        if times.count % 2 == 0 {
            return duration
        }
        else {
            // Last period has not ended, add time to current time
            return duration + NSDate.timeIntervalSinceReferenceDate - times.last!
        }
    }
}
