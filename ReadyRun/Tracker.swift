//
//  Tracker.swift
//  ReadyRun
//
//  Created by Daniel McMillan on 11/2/17.
//  Copyright © 2017 Daniel McMillan. All rights reserved.
//

import Foundation

/**
 Protocol for an activity tracker which populates TrackerData and is able to receive notifications about activity progress
*/
protocol Tracker {
    
    var data: TrackerData { get }
    
    /// Notify the tracker that the activity has started
    func activityStarted()
    
    /// Notify the tracker that the activity has paused
    func activityPaused()
    
    /// Notify the tracker that the activity has ended
    func activityEnded()
}
